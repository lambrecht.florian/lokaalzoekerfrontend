'use strict';
let lokalenLijst;

document.addEventListener('DOMContentLoaded', init);

function init() {
    getAllLokalen().then(r => {
        lokalenLijst = r;
        loadLokalen(lokalenLijst);
        document.querySelector("#search").addEventListener("keyup", searchOnInput);
    });
    getAllProvincies();
    document.querySelector("#goBack").addEventListener("click", function () { location.href = "index.html";});
}

async function getAllLokalen() {
    return fetch("https://api.lokaalzoeker.be/api/lokalen").then((response) => {
        return response.json();
    }).then(response => {
        return response;
    });

}

function getAllProvincies() {
    fetch("https://api.lokaalzoeker.be/api/provincies").then((response) => {
        return response.json();
    }).then(provincies => {
        provincies.forEach(p => loadProvincies(p))
    })
}

function loadLokalen(lokalen) {
    document.querySelector("#lokalen").innerHTML = "";
    lokalen.forEach(l => {
        document.querySelector("#lokalen").innerHTML += `
            <article>
                <img src="${l.img}" title="${l.naam}" alt="${l.naam}">
                <section>
                    <h3>Naam: ${l.naam} </h3>
                    <p>Locatie: ${l.locatie} </p>
                    <p>Provincie: ${l.provincie}</p>
                    <p>Aantal personen: ${l.personen} </p>
                </section>
            </article>`
    });
}

function loadProvincies(p) {
    document.querySelector("#provincie").innerHTML += `
        <option value="0">${p.provincie}</option>
    `
}

function searchOnInput() {
    let search = document.querySelector("#search").value;
    let output = [];
    lokalenLijst.forEach(l => {
       if (l.naam.toLowerCase().includes(search.toLowerCase())){
           output.push(l)
       }
       loadLokalen(output);
    });
}
