'use strict';

document.addEventListener('DOMContentLoaded', init);

function init() {
    if ("serviceWorker" in navigator) {
        navigator.serviceWorker.register("/sw.js").then(function (r) {
            console.info('Service worker registered successfully', r)
        }).catch(function (err) {
            console.info('Service worker registration failed: ', err)
        });
    }

    document.querySelector("#goBack").addEventListener("click", function () { location.href = "index.html";});
    document.querySelector("#Zoeken").addEventListener("click", function () {location.href = "zoeken.html"});
    document.querySelector("#mijnLokalen").addEventListener("click", function () {location.href = "index.html"});
    document.querySelector("#toevoegen").addEventListener("click", function () {location.href = "toevoegen.html"});
}
