"use strict";

importScripts("/assets/js/localforage.min.js");

const URLS_TO_CACHE = [
    "/",
    "/manifest.json",
    //HTML
    "/toevoegen.html",
    "/zoeken.html",
    "/index.html",
    //IMAGES
    "/images/bac.png",
    "/images/favicon.png",
    "/images/campfire.jpg",
    "/images/icons/key.png",
    "/images/icons/adjust.png",
    "/images/icons/back.png",
    "/images/icons/search.png",
    //CSS
    "/assets/css/index.css",
    "/assets/css/reset.css",
    "/assets/css/screen.css",
    "/assets/css/utilities/variables.css",
    "/assets/css/toevoegen.css",
    "/assets/css/zoeken.css",
    //JS
    "/assets/js/index.js",
    "/assets/js/localforage.min.js",
    "/assets/js/toevoegen.js",
    "/assets/js/zoeken.js",
    "/assets/js/script.js",
    //API
    "https://api.lokaalzoeker.be/api/lokalen",
    "https://api.lokaalzoeker.be/api/provincies"
];

const CACHE_NAME = "lokalenCache";

self.addEventListener("install", e => {
    e.waitUntil(
        caches.open(CACHE_NAME).then(cache => {
            return Promise.all[cache.addAll(URLS_TO_CACHE), initCache(cache)];
        })
    )
});

self.addEventListener("fetch", e => {
    e.respondWith(
        caches.open(CACHE_NAME).then(cache => {
            return cache.match(e.request);
        })
    )
});

function getLocalForage() {
    return localforage.createInstance( { name: "lokalenStorage" } );
}

function initCache(cache) {
    return fetch("https://api.lokaalzoeker.be/api/lokalen")
        .then(response => response.json())
        .then(json =>{
            getLocalForage().setItem("Lokalen", json);
        })
}
